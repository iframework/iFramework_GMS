﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData.Query;
using iFramework.Account.BLL;
using iFramework.Account.Contract;
using iFramework.Web.Api.Common;

namespace iFramework.Web.Api.Areas.Account
{
    public class UserApiController : WebApiControllerBase
    {
        
        static readonly IAccountService repository = new AccountService();
        
        
        /// <summary>
        /// 获取所有员工信息
        /// </summary>
        [WebApiOutputCache(30, 10, true)]
        [Queryable(AllowedQueryOptions = AllowedQueryOptions.All)]
        [DeflateCompression]
        public IEnumerable<User> GetAll(string Token)
        {
            
            var result = repository.GetUserList();
            base.Operater.Token = Guid.Parse(Token);
            return result;
        }

        

        /// <summary>
        /// 获取授权码
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [AllowAnonymous]
        public Guid Login(string username,string password)
        {
            var result = repository.Login(username, password,EnumLoginResourceType.WebApi).LoginToken;
            return result;
        }

        /// <summary>
        /// 注销授权码
        /// </summary>
        /// <param name="token">授权码</param>
        public void Logout(Guid token)
        {
            repository.Logout(token);
        }

        
    }
}
