﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.OData.Query;
using iFramework.Crm.BLL;
using iFramework.Crm.Contract;
using iFramework.Web.Api.Common;

namespace iFramework.Web.Api.Areas.Crm
{
    public class ParkingApiController : WebApiControllerBase
    {
        static readonly ICrmService repository = new CrmService();
        /// <summary>
        /// 传回所有的泊位数据
        /// 支持OData查询
        /// </summary>
        /// <returns>成功传回泊位对象集合数据</returns>
        [WebApiOutputCache(30, 10, true)]
        [Queryable(AllowedQueryOptions = AllowedQueryOptions.All)]
        [DeflateCompression]
        public IEnumerable<Parking> GetAll()
        {
            var result = this.CrmService.GetParkingList();
            return result;
        }
    }
}
