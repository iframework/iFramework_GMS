﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData.Query;
using iFramework.Crm.Contract;
using iFramework.Web.Api.Common;

namespace iFramework.Web.Api.Areas.Crm
{
    public class ProjectApiController : WebApiControllerBase
    {
        //static readonly ICrmService repository = new CrmService();

        private IEnumerable<Project> _projects;
        /// <summary>
        /// 传回所有的项目数据
        /// 支持OData查询
        /// </summary>
        /// <returns>成功传回项目对象集合数据</returns>
        //[Queryable(PageSize=1)]
        [WebApiOutputCache(30, 10, true)]
        [Queryable(AllowedQueryOptions = AllowedQueryOptions.All)]
        [DeflateCompression]
        public IEnumerable<Project> Get(string token)
        {
            _projects = this.CrmService.GetProjectList();
            return _projects;
        }


        /// <summary>
        /// 依据传入的项目数据oProd，新增项目数据
        /// </summary>
        /// <param name="project">从FromBody传入项目数据</param>
        /// <param name="token">用户授权码</param>
        /// <returns>返回项目创建信息</returns>
        [AcceptVerbs("POST")]
        //[DeflateCompression]
        public HttpResponseMessage Post(Project project, string token)
        {
            project.ID = -1;
            base.LoginInfo = this.AccountService.GetLoginInfo(Guid.Parse(token));
            base.Operater.Token = Guid.Parse(token);
            this.CrmService.SaveProject(project);
            var response = Request.CreateResponse<Project>(HttpStatusCode.Created, project);
            string uri = Url.Link("DefaultApi", new { id = project.ID });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        public string Put(int ID,string token)
        {
            return token;
        }
    }
}
