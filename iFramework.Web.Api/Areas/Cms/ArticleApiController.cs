﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData.Query;
using iFramework.Cms.Contract;
using iFramework.Framework.Utility;
using iFramework.Web.Api.Common;

namespace iFramework.Web.Api.Areas.Cms
{
    public class ArticleApiController : WebApiControllerBase
    {
        //static readonly ICmsService repository = new CmsService();

        /// <summary>
        /// 传回所有的泊位数据
        /// 支持OData查询
        /// </summary>
        /// <returns>成功传回泊位对象集合数据</returns>
        [WebApiOutputCache(30, 10, true)]
        [Queryable(AllowedQueryOptions = AllowedQueryOptions.All)]
        //[DeflateCompression]
        public IEnumerable<Article> GetAll()
        {
            List<Article> result = new List<Article>();
            var tmp = this.CmsService.GetArticleList();
            foreach (Article article in tmp)
            {
                article.Content = StringUtil.HtmlEncode(article.Content);
                result.Add(article);
            }
            return result;
        }

        [WebApiOutputCache(30, 10, true)]
        [Queryable(AllowedQueryOptions = AllowedQueryOptions.All)]
        [DeflateCompression]
        public Article GetArticleByID(int ID)
        {
            Article item = this.CmsService.GetArticle(ID);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }
    }
}