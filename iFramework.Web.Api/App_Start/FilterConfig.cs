﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Caching;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Security;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;

namespace iFramework.Web.Api
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new ValidationAttribute());
            //filters.Add(new RequireHttpsAttribute());
        }
    }

    public class RequireHttpsAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
                {
                    ReasonPhrase = "HTTPS Required"
                };
            }
            else
            {
                base.OnAuthorization(actionContext);
            }
        }
    }

    #region 数据验证
    /*
         * 实体数据验证
         * 1.修改实体模型:
         * public int Id { get; set; }
         * [Required]
         * [StringLength(20,ErrorMessage="名字太长了或者太短了",MinimumLength=4)]        
         * public string Name { get; set; }
              
         * [RegularExpression(@"([2-5]\d)",ErrorMessage="年龄在20-50之间")]
         * public int Age { get; set; }
         * 
         * 2.需要添加 System.ComponentModel.DataAnnotations; 并引用
         * 3.在webApiConfig注册Filter：config.Filters.Add(new Filters.ValidationAttribute());
         * 
         * 页面应用：
         * <input id="name" name="name" data-val="true" data-val-required="是不是忘记输入名称了?" type="text" value="" />
         * 页面JS:
         * $.validator.addMethod("failure", function () { return false; });
            $.validator.unobtrusive.adapters.addBool("failure");
            $.validator.unobtrusive.revalidate = function (form, validationResult) {
                $.removeData(form[0], 'validator');
                var serverValidationErrors = [];
                for (var property in validationResult) {
                    var elementId = property.toLowerCase();
                    elementId = elementId.substr(elementId.indexOf('.') + 1);
                    var item = form.find('#' + elementId);
                    serverValidationErrors.push(item);
                    item.attr('data-val-failure', validationResult[property][0]);
                    jQuery.validator.unobtrusive.parseElement(item[0]);
                }
                form.valid();
                $.removeData(form[0], 'validator');
                $.each(serverValidationErrors, function () {
                    this.removeAttr('data-val-failure');
                    jQuery.validator.unobtrusive.parseElement(this[0]);
                });
            }
         * JS增加服务器返回错误消息显示操作
         * 400 '/'*' BadRequest '*'/':function (jqxhr) {
                            var validationResult = $.parseJSON(jqxhr.responseText);
                            $.validator.unobtrusive.revalidate(form, validationResult.ModelState);
                        }
         */
    public class ValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest,
                    actionContext.ModelState);
            }
        }
    }
    #endregion

    #region 身份认证 未使用
    /// <summary>
    /// 基本验证Attribtue，用以Action的权限处理
    /// </summary>
    public class BasicAuthenticationAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 检查用户是否有该Action执行的操作权限
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //检验用户ticket信息，用户ticket信息来自调用发起方
            if (actionContext.Request.Headers.Authorization != null)
            {
                //解密用户ticket,并校验用户名密码是否匹配
                var encryptTicket = actionContext.Request.Headers.Authorization.Parameter;
                if (ValidateUserTicket(encryptTicket))
                    base.OnActionExecuting(actionContext);
                else
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }
            else
            {
                //检查web.config配置是否要求权限校验
                bool isRquired = (WebConfigurationManager.AppSettings["WebApiAuthenticatedFlag"].ToString() == "true");
                if (isRquired)
                {
                    //如果请求Header不包含ticket，则判断是否是匿名调用
                    var attr = actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().OfType<AllowAnonymousAttribute>();
                    bool isAnonymous = attr.Any(a => a is AllowAnonymousAttribute);

                    //是匿名用户，则继续执行；非匿名用户，抛出“未授权访问”信息
                    if (isAnonymous)
                        base.OnActionExecuting(actionContext);
                    else
                        actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
                else
                {
                    base.OnActionExecuting(actionContext);
                }
            }
        }

        /// <summary>
        /// 校验用户ticket信息
        /// </summary>
        /// <param name="encryptTicket"></param>
        /// <returns></returns>
        private bool ValidateUserTicket(string encryptTicket)
        {
            var userTicket = FormsAuthentication.Decrypt(encryptTicket);
            var userTicketData = userTicket.UserData;

            string userName = userTicketData.Substring(0, userTicketData.IndexOf(":"));
            string password = userTicketData.Substring(userTicketData.IndexOf(":") + 1);

            //检查用户名、密码是否正确，验证是合法用户
            //var isQuilified = CheckUser(userName, password);
            return true;
        }
    }
    #endregion

    #region 页面安全调用WebApi 未使用
    /* 1.扩展自定义身份验证:
         * 添加类 CustomAuthorizeAttribute.cs
           该类继承自System.Web.Http.AuthorizeAttribute（身份认证类）通过重写其身份认证核心方法来达到 web API 身份认证的效果。
         * 2.增加身份认证(必须登录后才能进行查询等操作)在Controller上加上属性
         * 3.编写登录方法
         * [HttpPost]
            public ActionResult Login(FormCollection fol)
            {
                ///此处为了演示简化登录过程
                ///可以在此处扩展验证用户名或者密码是否正确
                System.Web.Security.FormsAuthentication.SetAuthCookie(fol["username"], false);
                return Redirect("/HTMLPage5.htm");
            }
         * 
         * 4. 页面调用需注意
         * 有两个小地方做配置
         * (1)第一个就是web.config 配置form认证
         *     <authentication mode="Forms">
                  <forms loginUrl="~/home/Login" timeout="2880" />
               </authentication>
         * (2) 修改HTMLPage5.html的js(HTMLPage5.html可以直接复制HTMLPage4.html),将这段获取数据的代码修改为带验证身份进行跳转的
         *   原JS:
         *   $.get('/api/userInfo', function (data) {
                    // 从API中
                    // 得到返回的数据，更新 Knockout 模型并且绑定到页面UI模板中                         
                    viewModel.userinfos(data);
                });
         *    修改后：
         *    $.ajax({
                    url: '/api/userinfo',
                    type: 'GET',
                    contentType: 'application/json; charset=utf-8',
                    statusCode: {
                        200 '/'*'Created'*'/': function (data) {
                            viewModel.userinfos(data)
                        }, 401: function (jqXHR, textStatus, errorThrown) {
                            window.location.href = '/home/login';
                        }
                    }
                });
         */

    public class CustomAuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //判断用户是否登录
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                HandleUnauthorizedRequest(actionContext);
        }
        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var challengeMessage = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            challengeMessage.Headers.Add("WWW-Authenticate", "Basic");
            throw new System.Web.Http.HttpResponseException(challengeMessage);

        }

    }
    #endregion

    #region 数据压缩(减少响应包的大小和响应速度，压缩是一种简单而有效的方式，手机调用时节省流量)
    /*
         * 使用的时候，使用属性标签即可
         * [DeflateCompression]
            public string Get(int id)
            {
                return "ok"+id;
            }
         */
    public class DeflateCompressionAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuted(HttpActionExecutedContext actContext)
        {
            var content = actContext.Response.Content;
            var bytes = content == null ? null : content.ReadAsByteArrayAsync().Result;
            var zlibbedContent = bytes == null ? new byte[0] :
            CompressionHelper.DeflateByte(bytes);
            actContext.Response.Content = new ByteArrayContent(zlibbedContent);
            actContext.Response.Content.Headers.Remove("Content-Type");
            actContext.Response.Content.Headers.Add("Content-encoding", "gzip");
            actContext.Response.Content.Headers.Add("Content-Type", "application/json");
            base.OnActionExecuted(actContext);
        }
    }

    /*
     * 如果使用GZIP压缩的话，只需要将
     * new Ionic.Zlib.DeflateStream( 改为
     * new Ionic.Zlib.GZipStream(，然后
     * actContext.Response.Content.Headers.Add("Content-encoding", "deflate");改为
     * actContext.Response.Content.Headers.Add("Content-encoding", "gzip");
     */
    public class CompressionHelper
    {
        public static byte[] DeflateByte(byte[] str)
        {
            if (str == null)
            {
                return null;
            }
            using (var output = new MemoryStream())
            {
                using (
                  var compressor = new Ionic.Zlib.GZipStream(
                  output, Ionic.Zlib.CompressionMode.Compress,
                  Ionic.Zlib.CompressionLevel.BestSpeed))
                {
                    compressor.Write(str, 0, str.Length);
                }

                return output.ToArray();
            }
        }
    }
    #endregion

    #region 缓存应用
    /*
     * 简单缓存客户端
     * 应用方法：在需要缓存的控制器上添加属性标签
     * [CacheClient(Duration = 120)]//Duration为缓存时间秒数
     */
    public class CacheClientAttribute : ActionFilterAttribute
    {
        public int Duration { get; set; }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            actionExecutedContext.Response.Headers.CacheControl = new CacheControlHeaderValue
            {
                MaxAge = TimeSpan.FromSeconds(Duration),
                MustRevalidate = true,
                Public = true
            };
        }
    }

    /// <summary>
    /// 缓存高级应用
    /// 使用示例：[WebApiOutputCache(120, 60, false)]//120:表示将Action的结果在服务器上保存120秒；60：表示将结果在客户端保存60秒；false：表示不对所有/匿名用户使用缓存
    /// </summary>
    public class WebApiOutputCacheAttribute : ActionFilterAttribute
    {
        // cache length in seconds
        private int _timespan;
        // client cache length in seconds
        private int _clientTimeSpan;
        // cache for anonymous users only?
        private bool _anonymousOnly;
        // cache key
        private string _cachekey;
        // cache repository
        private static readonly ObjectCache WebApiCache = MemoryCache.Default;
        public WebApiOutputCacheAttribute(int timespan, int clientTimeSpan, bool anonymousOnly)
        {
            _timespan = timespan;
            _clientTimeSpan = clientTimeSpan;
            _anonymousOnly = anonymousOnly;
        }

        private bool _isCacheable(HttpActionContext ac)
        {
            if (_timespan > 0 && _clientTimeSpan > 0)
            {
                if (_anonymousOnly)
                    if (Thread.CurrentPrincipal.Identity.IsAuthenticated)
                        return false;
                if (ac.Request.Method == HttpMethod.Get) return true;
            }
            else
            {
                throw new InvalidOperationException("Wrong Arguments");
            }
            return false;
        }

        private CacheControlHeaderValue setClientCache()
        {
            var cachecontrol = new CacheControlHeaderValue();
            cachecontrol.MaxAge = TimeSpan.FromSeconds(_clientTimeSpan);
            cachecontrol.MustRevalidate = true;
            return cachecontrol;
        }

        public override void OnActionExecuting(HttpActionContext ac)
        {
            if (ac != null)
            {
                if (_isCacheable(ac))
                {
                    _cachekey = string.Join(":", new string[] { ac.Request.RequestUri.AbsolutePath, ac.Request.Headers.Accept.FirstOrDefault().ToString() });
                    if (WebApiCache.Contains(_cachekey))
                    {
                        var val = (string)WebApiCache.Get(_cachekey);
                        if (val != null)
                        {
                            ac.Response = ac.Request.CreateResponse();
                            ac.Response.Content = new StringContent(val);
                            var contenttype = (MediaTypeHeaderValue)WebApiCache.Get(_cachekey + ":response-ct");
                            if (contenttype == null)
                                contenttype = new MediaTypeHeaderValue(_cachekey.Split(':')[1]);
                            ac.Response.Content.Headers.ContentType = contenttype;
                            ac.Response.Headers.CacheControl = setClientCache();
                            return;
                        }
                    }
                }
            }
            else
            {
                throw new ArgumentNullException("actionContext");
            }
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (!(WebApiCache.Contains(_cachekey)))
            {
                var body = actionExecutedContext.Response.Content.ReadAsStringAsync().Result;
                WebApiCache.Add(_cachekey, body, DateTime.Now.AddSeconds(_timespan));
                WebApiCache.Add(_cachekey + ":response-ct", actionExecutedContext.Response.Content.Headers.ContentType, DateTime.Now.AddSeconds(_timespan));
            }
            if (_isCacheable(actionExecutedContext.ActionContext))
                actionExecutedContext.ActionContext.Response.Headers.CacheControl = setClientCache();
        }
    } 
    #endregion


}

































