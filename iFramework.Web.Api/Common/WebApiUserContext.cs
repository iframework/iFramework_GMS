﻿using System;
using iFramework.Core.Cache;

namespace iFramework.Web.Api.Common
{
    public class WebApiUserContext : UserContext
    {
        public WebApiUserContext()
            : base(WebApiCookieContext.Current)
        {
        }

        public WebApiUserContext(IAuthCookie authCookie)
            : base(authCookie)
        {
        }

        public static WebApiUserContext Current
        {
            get
            {
                return CacheHelper.GetItem<WebApiUserContext>();
            }
        }
    }
}
