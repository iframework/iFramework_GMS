﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iFramework.Core.Cache;
using iFramework.Framework.Utility;

namespace iFramework.Web.Api.Common
{
    public class WebApiCookieContext : CookieContext
    {
        public static WebApiCookieContext Current
        {
            get
            {
                return CacheHelper.GetItem<WebApiCookieContext>();
            }
        }

        public override string KeyPrefix
        {
            get
            {
                return Fetch.ServerDomain + "_WebApiContext_";
            }
        }
    }
}
