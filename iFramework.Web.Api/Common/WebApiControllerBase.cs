﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using iFramework.Account.BLL;
using iFramework.Account.Contract;
using iFramework.Core.Config;
using iFramework.Framework.Contract;
using iFramework.Framework.Utility;

namespace iFramework.Web.Api.Common
{
    public abstract class WebApiControllerBase : ApiControllerBase
    {
        public CachedConfigContext ConfigContext
        {
            get { return CachedConfigContext.Current; }
        }

        Operater _operater=new Operater();

        /// <summary>
        /// 操作人，为了记录操作历史
        /// </summary>
        public override Operater Operater
        {
            get
            {
                
                _operater= new Operater()
                {
                    Name = this.LoginInfo == null ? string.Empty : this.LoginInfo.LoginName,
                    Token = this.LoginInfo == null ? Guid.Empty : this.LoginInfo.LoginToken,
                    UserId = this.LoginInfo == null ? 0 : this.LoginInfo.UserID,
                    Time = DateTime.Now,
                    IP = Fetch.UserIp
                };
                if (string.IsNullOrEmpty(this.LoginInfo.LoginName) && this.LoginInfo.LoginToken!=Guid.Empty)
                    this.LoginInfo = new AccountService().GetLoginInfo(this.LoginInfo.LoginToken);
                _operater.Name = this.LoginInfo == null ? string.Empty : this.LoginInfo.LoginName;
                _operater.UserId = this.LoginInfo == null ? 0 : this.LoginInfo.UserID;
                _operater.EnumLoginResourceType = this.LoginInfo == null ? string.Empty : this.LoginInfo.EnumLoginResourceType;
                WCFContext.Current.Operater = _operater;
                return _operater;
            }
        }

        /// <summary>
        /// 用户Token，每次页面都会把这个UserToken标识发送到服务端认证
        /// </summary>
        //public virtual Guid UserToken
        //{
        //    get { return CookieContext.UserToken; }
        //}

        /// <summary>
        /// 登录后用户信息
        /// </summary>
        public virtual LoginInfo LoginInfo { get;set; }

        /// <summary>
        /// 登录后用户信息里的用户权限
        /// </summary>
        public virtual List<EnumBusinessPermission> PermissionList
        {
            get
            {
                var permissionList = new List<EnumBusinessPermission>();

                if (this.LoginInfo != null)
                    permissionList = this.LoginInfo.BusinessPermissionList;

                return permissionList;
            }
        }

        #region  Common Pager
        /// <summary>
        ///  给结果集扩展一个分页
        /// </summary>
        /// <typeparam name="T">实体</typeparam>
        /// <param name="_defaultSetting">页面设置</param>
        /// <param name="options">筛选条件集合</param>
        /// <param name="_list">待分页集合</param>
        /// <returns></returns>
        [NonAction]
        public PageResult<TEntity> GetPager<TEntity>(ODataQueryOptions<TEntity> options, IEnumerable<TEntity> _list, int PageSizeCount = 6) where TEntity : class
        {
            ODataQuerySettings settings = new ODataQuerySettings
            {
                PageSize = PageSizeCount
            };
            IQueryable results = options.ApplyTo(_list.AsQueryable(), settings);
            Request.GetInlineCount();
            return new PageResult<TEntity>(
                results as IEnumerable<TEntity>,
                Request.GetNextPageLink(),
                Request.GetInlineCount());
        }
        #endregion

        #region Common Upload
        /// <summary>
        /// 通用上传图片-操作
        /// </summary>
        /// <typeparam name="T">上传实体</typeparam>
        /// <param name="dirPath">上传路径</param>
        /// <param name="fileNameAction">文件名自定义扩展</param>
        /// <param name="Entity">实体名字</param>
        /// <returns>图片上传是否成功</returns>
        [NonAction]
        public Hashtable CommonUpload<T>(string dirPath, Action<string> fileNameAction, out T Entity) where T : class
        {
            var queryp = Request.GetQueryNameValuePairs(); //获得查询字符串的键值集合 
            Entity = GetUploadEntity<T>(queryp); //实体赋值操作

            // 检查是否是 multipart/form-data 
            if (!Request.Content.IsMimeMultipartContent("form-data"))
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            //文件保存目录路径 
            string SaveTempPath = dirPath;
            String dirTempPath = HttpContext.Current.Server.MapPath(SaveTempPath);
            ////Logger.Error("文件路径:" + dirTempPath);
            // 设置上传目录 
            var provider = new MultipartFormDataStreamProvider(dirTempPath);

            //Logger.Error("queryp参数:" + param);
            var task = Request.Content.ReadAsMultipartAsync(provider).
                ContinueWith<Hashtable>(o =>
                {
                    Hashtable hash = new Hashtable();
                    hash["error"] = 1;
                    hash["errmsg"] = "上传出错";
                    var file = provider.FileData[0];//provider.FormData 
                    string orfilename = file.Headers.ContentDisposition.FileName.TrimStart('"').TrimEnd('"');
                    FileInfo fileinfo = new FileInfo(file.LocalFileName);

                    ////Logger.Error("最大文件大小:" + fileinfo.Length);
                    ////Logger.Error("最大格式:" + orfilename);
                    //最大文件大小 
                    int maxSize = 10000000;
                    if (fileinfo.Length <= 0)
                    {
                        hash["error"] = 1;
                        hash["errmsg"] = "请选择上传文件。";
                    }
                    else if (fileinfo.Length > maxSize)
                    {
                        hash["error"] = 1;
                        hash["errmsg"] = "上传文件大小超过限制。";
                    }
                    else
                    {
                        string fileExt = orfilename.Substring(orfilename.LastIndexOf('.'));
                        //定义允许上传的文件扩展名 
                        String fileTypes = "gif,jpg,jpeg,png,bmp";
                        if (String.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
                        {
                            hash["error"] = 1;
                            hash["errmsg"] = "上传文件扩展名是不允许的扩展名。";
                        }
                        else
                        {
                            //String ymd = DateTime.Now.ToString("yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                            //String newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                            //String finalFileName = newFileName + fileExt;
                            string excute_FileName = string.Empty;
                            fileNameAction = (i) => { excute_FileName = i; };
                            fileinfo.CopyTo(Path.Combine(dirTempPath, excute_FileName), true);
                            fileinfo.Delete();
                            hash["error"] = 0;
                            hash["errmsg"] = "上传成功";
                            hash["filePathUrl"] = excute_FileName;
                        }
                    }
                    return hash;
                });
            return null;
        }

        /// <summary>
        /// 反射动态的实体赋值-操作
        /// </summary>
        /// <typeparam name="Entity"></typeparam>
        /// <param name="queryp"></param>
        /// <returns></returns>
        [NonAction]
        public Entity GetUploadEntity<Entity>(IEnumerable<KeyValuePair<string, string>> queryp) where Entity : class
        {
            var entity = typeof(Entity);
            Object model = entity.Assembly.CreateInstance(entity.FullName, true);
            var props = entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (PropertyInfo propertyInfo in typeof(Entity).GetProperties())
            {
                foreach (var keyValuePair in queryp.Where(keyValuePair => keyValuePair.Key.Equals(propertyInfo.Name.ToString())))
                {
                    propertyInfo.SetValue(model, keyValuePair.Value, null);
                }
            }
            return (Entity)model;
        }
        #endregion

    }
}