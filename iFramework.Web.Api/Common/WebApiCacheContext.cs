﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iFramework.Core.Cache;
using iFramework.Framework.Utility;
using iFramework.Crm.Contract;

namespace iFramework.Web.Api.Common
{
    public class WebApiCacheContext 
    {
        public static WebApiCacheContext Current
        {
            get
            {
                return CacheHelper.GetItem<WebApiCacheContext>();
            }
        }

        public Dictionary<int, City> CityDic
        {
            get
            {
                return CacheHelper.Get<Dictionary<int, City>>("CityDic", () =>
                {
                    return ServiceContext.Current.CrmService.GetCityList().ToDictionary(a => a.ID);
                });
            }
        }

        public Dictionary<int, Area> AreaDic
        {
            get
            {
                return CacheHelper.Get<Dictionary<int, Area>>("AreaDic", () =>
                {
                    return ServiceContext.Current.CrmService.GetAreaList().ToDictionary(a => a.ID);
                });
            }
        }
    }
}
