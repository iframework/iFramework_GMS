﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using iFramework.Framework.Contract;
using iFramework.Framework.Utility;

namespace iFramework.Crm.Contract
{
    [Auditable]//Auditable属性用于该表在由CUD操作时，将操作记录存到数据库
    [Table("Parking")]
    public partial class Parking : ModelBase
    {
        [StringLength(100)]
        public string ParkingName { get; set; }
        public int BerthNum { get; set; } 
        public int ParkingType { get; set; }
        public int CostAreaType { get; set; } 
        public decimal? ParkingLng { get; set; }
        public decimal? ParkingLat { get; set; } 
        public long RegionId { get; set; }

        [StringLength(400, ErrorMessage = "泊站描述不能超过400个字")]
        public string ParkingDesc { get; set; } 
        public bool IsActive { get; set; }
        [StringLength(100)]
        public string PlaceName { get; set; }
        [StringLength(50)]
        public string ParkingNo { get; set; }
    }

    /// <summary>
    /// 泊站类型
    /// </summary>
    public enum EnumParkingType
    {
        [EnumTitle("无", IsDisplay = false)]
        None = 0,

        [EnumTitle("道路泊站")]
        Road = 1,

        [EnumTitle("自行平面泊站")]
        Plaza = 2,

        [EnumTitle("楼房式立体泊站")]
        Building = 3,

        [EnumTitle("钢装式立体泊站")]
        SteelMounted = 4,

        [EnumTitle("机械式泊站")]
        Mechanical = 5
    }

    /// <summary>
    /// 泊站收费类型
    /// </summary>
    public enum EnumCostAreaType
    {
        [EnumTitle("无", IsDisplay = false)]
        None = 0,

        [EnumTitle("I类中心区域")]
        CostAreaIByCenter = 1,

        [EnumTitle("I类次中心区域")]
        CostAreaIByCentral = 2,

        [EnumTitle("II类区域")]
        CostAreaII = 3,
    }
}
