﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using iFramework.Framework.Contract;

namespace iFramework.Crm.Contract
{
    [Auditable]//Auditable属性用于该表在由CUD操作时，将操作记录存到数据库
     [Table("Area")]
    public class Area : ModelBase
    {
        [Required(ErrorMessage = "名称不能为空")]
        [StringLength(50)]
        public string Name { get; set; }

        public int CityId { get; set; }
    }
   
}
