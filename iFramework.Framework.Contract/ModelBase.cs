﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iFramework.Framework.Contract
{
    [SerializableAttribute]
    public class ModelBase
    {
        private int _id;
        private DateTime _createTime;
        public ModelBase()
        {
            CreateTime = DateTime.Now;
        }
        
        public virtual int ID {
            get { return _id; }
            set { _id = value; }
        }
        public virtual DateTime CreateTime {
            get { return _createTime; }
            set { _createTime = value; }
        }

        
    }
}
