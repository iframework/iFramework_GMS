﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iFramework.Account.Contract;
using iFramework.Crm.Contract;
using iFramework.Framework.Contract;
using iFramework.Framework.Utility;
using iFramework.Web.Admin.Common;

namespace iFramework.Web.Admin.Areas.Crm.Controllers
{
    [Permission(EnumBusinessPermission.CrmManage_Parking)]
    public class ParkingController : AdminControllerBase
    {
        //
        // GET: /Crm/Parking/

        public ActionResult Index(ParkingRequest request)
        {
            this.TryUpdateModel<Parking>(request.Parking);

            this.ModelState.Clear();
            this.RenderMyViewData(request.Parking, true);
            var result = this.CrmService.GetParkingList(request);
            return View(result);
        }


        public ActionResult Create()
        {
            var model = new Parking() { };
            this.RenderMyViewData(model);
            return View("Edit", model);
        }

        //
        // POST: /Crm/Project/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            var model = new Parking();
            this.TryUpdateModel<Parking>(model);

            try
            {
                this.CrmService.SaveParking(model);
            }
            catch (BusinessException e)
            {
                this.ModelState.AddModelError(e.Name, e.Message);
                this.RenderMyViewData(model);
                return View("Edit", model);
            }

            return this.RefreshParent();
        }

        //
        // GET: /Crm/Project/Edit/5

        public ActionResult Edit(int id)
        {
            var model = this.CrmService.GetParking(id);
            this.RenderMyViewData(model);
            return View(model);
        }

        //
        // POST: /Crm/Project/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            var model = this.CrmService.GetParking(id);
            this.TryUpdateModel<Parking>(model);

            try
            {
                this.CrmService.SaveParking(model);
            }
            catch (BusinessException e)
            {
                this.ModelState.AddModelError(e.Name, e.Message);
                this.RenderMyViewData(model);
                return View("Edit", model);
            }

            return this.RefreshParent();
        }

        public ActionResult ManageBerth(int id)
        {
            var model = this.CrmService.GetParking(id);
            this.RenderMyViewData(model);
            return PartialView("ManageBerth");
        }

        //
        // POST: /Crm/Project/Edit/5

        [HttpPost]
        public ActionResult ManageBerth(int id, FormCollection collection)
        {
            var model = this.CrmService.GetParking(id);
            this.TryUpdateModel<Parking>(model);

            try
            {
                this.CrmService.SaveParking(model);
            }
            catch (BusinessException e)
            {
                this.ModelState.AddModelError(e.Name, e.Message);
                this.RenderMyViewData(model);
                return View("Edit", model);
            }

            return this.RefreshParent();
        }

        // POST: /Crm/VisitRecord/Delete/5

        [HttpPost]
        public ActionResult Delete(List<int> ids)
        {
            this.CrmService.DeleteVisitRecord(ids);
            return RedirectToAction("Index");
        }

        private void RenderMyViewData(Parking model, bool isBasic = false)
        {
            ViewData.Add("ParkingType", new SelectList(EnumHelper.GetItemValueList<EnumParkingType>(), "Key", "Value", model.ParkingType));
            ViewData.Add("CostAreaType", new SelectList(EnumHelper.GetItemValueList<EnumCostAreaType>(), "Key", "Value", model.CostAreaType));
        }
    }
}
