﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using iFramework.Sys.Contract;

namespace iFramework.Web.Admin.Common
{
    public class AdminDocProvider : IDocumentationProvider
    {
        public string GetDocumentation(HttpParameterDescriptor parameterDescriptor)
        {
            string doc = "";
            var attr = parameterDescriptor.ActionDescriptor
                        .GetCustomAttributes<ApiModel.ApiParameterDocAttribute>()
                        .Where(p => p.Parameter == parameterDescriptor.ParameterName)
                        .FirstOrDefault();
            if (attr != null)
            {
                doc = attr.Documentation;
            }
            return doc;
        }
        public string GetDocumentation(HttpActionDescriptor actionDescriptor)
        {
            string doc = "";
            var attr = actionDescriptor.GetCustomAttributes<ApiModel.ApiDocAttribute>().FirstOrDefault();
            if (attr != null)
            {
                doc = attr.Documentation;
            }
            return doc;
        }

        #region IDocumentationProvider 成员


        public string GetDocumentation(HttpControllerDescriptor controllerDescriptor)
        {
            string doc=string.Empty;
            var attr = controllerDescriptor.GetCustomAttributes<ApiModel.ApiDocAttribute>().FirstOrDefault();
            if (attr != null)
            {
                doc = attr.Documentation;
            }
            return doc;
        }

        public string GetResponseDocumentation(HttpActionDescriptor actionDescriptor)
        {
            return string.Empty;
            throw new NotImplementedException();
        }

        #endregion
    }
}