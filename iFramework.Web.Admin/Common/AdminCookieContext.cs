﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iFramework.Core.Cache;
using iFramework.Framework.Utility;

namespace iFramework.Web.Admin.Common
{
    public class AdminCookieContext : CookieContext
    {
        public static AdminCookieContext Current
        {
            get
            {
                return CacheHelper.GetItem<AdminCookieContext>();
            }
        }

        public override string KeyPrefix
        {
            get
            {
                return Fetch.ServerDomain + "_AdminContext_";
            }
        }
    }
}
