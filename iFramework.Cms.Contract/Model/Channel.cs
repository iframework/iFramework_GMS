﻿using System;
using System.Linq;
using iFramework.Framework.Contract;
using System.Collections.Generic;
using iFramework.Framework.Utility;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace iFramework.Cms.Contract
{
    [Auditable]//Auditable属性用于该表在由CUD操作时，将操作记录存到数据库
    [Serializable]
    [Table("Channel")]
    public class Channel : ModelBase
    {
        
        public Channel()
        {
 
        }

        private string _name;

        [StringLength(100)]
        [Required]
        public string Name {
            get { return _name; }
            set { _name = value; }
        }

        private string _coverPicture;

        [StringLength(300)]
        public string CoverPicture
        {
            get { return _coverPicture; }
            set { _coverPicture = value; }
        }

        private string _desc;
        [StringLength(300)]
        public string Desc {
            get { return _desc; }
            set { _desc = value; }
        }

        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }

        }

        private int _hits;

        public int Hits
        {
            get { return _hits; }
            set { _hits = value; }
        }

        private List<Article> _articles;

        public virtual List<Article> Articles
        {
            get { return _articles; }
            set { _articles = value; }
        }
    }
}
