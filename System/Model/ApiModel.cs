﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Description;

namespace iFramework.Sys.Contract
{

    public class ApiModel
    {
        /*
         *  [ApiExplorerSettings(IgnoreApi=true)] //为了从ApiExplorer中隐藏一个API，将ApiExplorerSettings注解属性添加到该动作，并将IgnoreApi设置为true：
            public HttpResponseMessage Get(int id) 
            { 
            }
         */
        readonly IApiExplorer _explorer;
        public ApiModel(IApiExplorer explorer)
        {
            if (explorer == null)
            {
                throw new ArgumentNullException("explorer");
            }
            _explorer = explorer;
        }
        public ILookup<string, ApiDescription> GetApis()
        {
            return _explorer.ApiDescriptions.ToLookup(
                api => api.ActionDescriptor.ControllerDescriptor.ControllerName);
        }

        [AttributeUsage(AttributeTargets.Method)]
        public class ControllerDocAttribute : Attribute
        {
            public ControllerDocAttribute(string doc)
            {
                Documentation = doc;
            }
            public string Documentation { get; set; }
        }

        [AttributeUsage(AttributeTargets.Method)]
        public class ApiDocAttribute : Attribute
        {
            public ApiDocAttribute(string doc)
            {
                Documentation = doc;
            }
            public string Documentation { get; set; }
        }
        [AttributeUsage(AttributeTargets.Method)]
        public class ApiParameterDocAttribute : Attribute
        {
            public ApiParameterDocAttribute(string param, string doc)
            {
                Parameter = param;
                Documentation = doc;
            }
            public string Parameter { get; set; }
            public string Documentation { get; set; }
        }

    }
}
