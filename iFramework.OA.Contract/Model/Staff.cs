﻿using System;
using System.Linq;
using iFramework.Framework.Contract;
using System.Collections.Generic;
using iFramework.Framework.Utility;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace iFramework.OA.Contract
{
    [Auditable]//Auditable属性用于该表在由CUD操作时，将操作记录存到数据库
    [Serializable]
    [Table("Staff")]
    public class Staff : ModelBase
    {
        public Staff()
        {
            this.BranchId = 0;
        }

        private string _name;
        [StringLength(100)]
        [Required]
        public string Name {
            get { return _name; }
            set { _name = value; }
        }

        private string _coverPicture;
        [StringLength(300)]
        public string CoverPicture {
            get { return _coverPicture; }
            set { _coverPicture = value; }
        }

        private int _gender;
        public int Gender {
            get { return _gender; }
            set { _gender = value; }
        }

        private int _position;
        public int Position {
            get { return _position; }
            set { _position = value; }
        }

        private DateTime? _birthDate;

        public DateTime? BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        private string _tel;
        [StringLength(50, ErrorMessage = "电话不能超过50个字")]
        public string Tel {
            get { return _tel; }
            set { _tel = value; }
        }

        private string _email;
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "电子邮件地址无效")]
        public string Email {
            get { return _email; }
            set { _email = value; }
        }

        private string _address;
        [StringLength(100, ErrorMessage = "地址不能超过100个字")]
        public string Address {
            get { return _address; }
            set { _address = value; }
        }

        private int? _branchId;
        public int? BranchId {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private Branch _branch;
        public virtual Branch Branch {
            get { return _branch; }
            set { _branch = value; }
        }
    }

    /// <summary>
    /// 性别
    /// </summary>
    public enum EnumGender
    {
        [EnumTitle("男")]
        Man = 1,

        [EnumTitle("女")]
        Woman = 2
    }

     /// <summary>
    /// 职位
    /// </summary>
    public enum EnumPosition
    {
        [EnumTitle("无", IsDisplay = false)]
        None = 0,

        [EnumTitle("开发工程师")]
        Development = 1,

        [EnumTitle("高级开发工程师")]
        SDE = 2,

        [EnumTitle("测试工程师")]
        Testing = 3,

        [EnumTitle("项目经理")]
        PM = 4,
    }

}
