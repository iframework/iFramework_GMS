﻿using System;
using System.Linq;
using iFramework.Framework.Contract;
using System.Collections.Generic;
using iFramework.Framework.Utility;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace iFramework.OA.Contract
{
    [Auditable]//Auditable属性用于该表在由CUD操作时，将操作记录存到数据库
    [Serializable]
    [Table("Branch")]
    public class Branch : ModelBase
    {

        public Branch()
        {

        }

        private string _name;
        [StringLength(100)]
        [Required]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _desc;
        [StringLength(300)]
        public string Desc
        {
            get { return _desc; }
            set { _desc = value; }
        }

        private List<Staff> _staves;
        public virtual List<Staff> Staffs
        {
            get { return _staves; }
            set { _staves = value; }
        }

        private int _parentId;
        public int ParentId
        {
            get { return _parentId; }
            set { _parentId = value; }
        }

        private Branch _parentBranch;
        public virtual Branch ParentBranch
        {
            get { return _parentBranch; }
            set { _parentBranch = value; }
        }

        private List<Branch> _embranchment;
        [ForeignKey("ParentId")]
        public virtual List<Branch> Embranchment
        {
            get { return _embranchment; }
            set { _embranchment = value; }
        }
    }
}
