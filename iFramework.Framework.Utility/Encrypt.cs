﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace iFramework.Framework.Utility
{
    /// <summary>
    /// 加密字符串辅助类
    /// </summary>
    public class Encrypt
    {
        private const int KEY_SIZE = 192;
        private const string DEFAULT_KEY = "&hd22*DW@132";
        private const string DEFAULT_IV = "BB(3';.321M";

        /// <summary>
        /// MD5 hash加密
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string MD5(string s)
        {
            var md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            var result = BitConverter.ToString(md5.ComputeHash(UnicodeEncoding.UTF8.GetBytes(s.Trim())));
            return result;
        }


        #region DES加密解密
        //默认密钥向量
        private static byte[] keys = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

        /// <summary>
        /// DES加密字符串
        /// </summary>
        /// <param name="encryptString">待加密的字符串</param>
        /// <param name="encryptKey">加密密钥,要求为8位</param>
        /// <returns>加密成功返回加密后的字符串,失败返回源串</returns>
        /// private const string DEFAULT_KEY    = "&hd22*DW@132";
        /// private const string DEFAULT_IV = "BB(3';.321M";
        public static string Encode(string encryptString, string encryptKey = DEFAULT_KEY)
        {
            encryptKey = encryptKey.Substring(0, 8);
            encryptKey = encryptKey.PadRight(8, ' ');
            byte[] rgbKey = Encoding.UTF8.GetBytes(encryptKey.Substring(0, 8));
            byte[] rgbIV = keys;
            byte[] inputByteArray = Encoding.UTF8.GetBytes(encryptString);
            DESCryptoServiceProvider dCSP = new DESCryptoServiceProvider();
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, dCSP.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
            cStream.Write(inputByteArray, 0, inputByteArray.Length);
            cStream.FlushFinalBlock();
            return Convert.ToBase64String(mStream.ToArray());
        }

        /// <summary>
        /// DES解密字符串
        /// </summary>
        /// <param name="decryptString">待解密的字符串</param>
        /// <param name="decryptKey">解密密钥,要求为8位,和加密密钥相同</param>
        /// <returns>解密成功返回解密后的字符串,失败返源串</returns>
        public static string Decode(string decryptString, string decryptKey = DEFAULT_KEY)
        {
            try
            {
                decryptKey = decryptKey.Substring(0, 8);
                decryptKey = decryptKey.PadRight(8, ' ');
                byte[] rgbKey = Encoding.UTF8.GetBytes(decryptKey);
                byte[] rgbIV = keys;
                byte[] inputByteArray = Convert.FromBase64String(decryptString);
                DESCryptoServiceProvider DCSP = new DESCryptoServiceProvider();

                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, DCSP.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch
            {
                return "";
            }


        }
        #endregion


        #region My
        private static byte[] Format(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            if (buffer.Length <= 8)
            {
                text = text.PadRight(9 - buffer.Length + text.Length, '$');
                buffer = Encoding.UTF8.GetBytes(text);
            }
            return buffer;
        }

        private static byte[] Format(byte[] buffer, int length)
        {
            byte[] result = new byte[length / 8];
            int index = 0;
            for (index = 0; index < buffer.Length; index++)
                result[index] = buffer[index];
            for (; index < result.Length; index++)
                result[index] = 0x40;
            return result;
        }

        /// <summary>
        /// Use the 3DES to encrypt text with default encode(UTF-16)
        /// </summary>
        /// <param name="buffer">Text buffer</param>
        /// <param name="key">Key</param>
        /// <param name="iv">IV</param>
        /// <returns>Encrypted</returns>
        public static string TripleDESEncrypt(byte[] buffer, byte[] key, byte[] iv)
        {
            //Tool.License._Authenticate();
            StringBuilder result = null;
            MemoryStream ms = new MemoryStream();
            try
            {
                result = new StringBuilder();
                TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
                des.KeySize = KEY_SIZE;
                ICryptoTransform transform = des.CreateEncryptor(Format(key, KEY_SIZE), Format(iv, KEY_SIZE));
                CryptoStream cs = new CryptoStream(ms, transform, CryptoStreamMode.Write);
                cs.Write(buffer, 0, buffer.Length);
                cs.FlushFinalBlock();
                foreach (byte b in ms.ToArray()) result.AppendFormat("{0:X2}", b);
            }
            catch
            {
                result = null;
            }
            finally
            {
                ms.Close();
                ms.Dispose();
            }
            return result == null ? null : result.ToString();
        }

        /// <summary>
        /// Use the 3DES to encrypt text with default encode(UTF-16)
        /// </summary>
        /// <param name="buffer">Text buffer</param>
        /// <param name="key">Key</param>
        /// <param name="iv">IV</param>
        /// <returns>Encrypted</returns>
        public static string TripleDESEncrypt(byte[] buffer, string key, string iv)
        {
            return TripleDESEncrypt(buffer
                , Format(key)
                , Format(iv));
        }

        /// <summary>
        /// Decrypt encrypted with default encode(UTF-16) in 3DES
        /// </summary>
        /// <param name="encrypted">Encrypted text content</param>
        /// <param name="key">Key</param>
        /// <param name="iv">IV</param>
        /// <returns>Byte array of decryption</returns>
        public static byte[] TripleDESDecrypt(string encrypted, byte[] key, byte[] iv)
        {
            //Tool.License._Authenticate();
            byte[] result = null;
            MemoryStream ms = new MemoryStream();
            try
            {
                TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
                byte[] buffer = new byte[encrypted.Length / 2];
                for (int x = 0; x < encrypted.Length / 2; x++)
                {
                    int i = (System.Convert.ToInt32(encrypted.Substring(x * 2, 2), 16));
                    buffer[x] = (byte)i;
                }
                des.KeySize = KEY_SIZE;
                ICryptoTransform transform = des.CreateDecryptor(Format(key, KEY_SIZE), Format(iv, KEY_SIZE));
                CryptoStream cs = new CryptoStream(ms, transform, CryptoStreamMode.Write);
                cs.Write(buffer, 0, buffer.Length);
                cs.FlushFinalBlock();
                result = ms.ToArray();
            }
            catch
            {
                result = null;
            }
            finally
            {
                ms.Close();
                ms.Dispose();
            }
            return result;
        }

        /// <summary>
        /// Decrypt encrypted with default encode(UTF-16) in 3DES
        /// </summary>
        /// <param name="encrypted">Encrypted text content</param>
        /// <param name="key">Key</param>
        /// <param name="iv">IV</param>
        /// <returns>Byte array of decryption</returns>
        public static byte[] TripleDESDecrypt(string encrypted, string key, string iv)
        {
            return TripleDESDecrypt(encrypted
                , Format(key)
                , Format(iv));
        }

        /// <summary>
        /// Use the 3DES to encrypt text with default key
        /// </summary>
        /// <param name="text">Text content</param>
        /// <param name="encoding">Encoding</param>
        /// <returns>Encrypted</returns>
        public static string DefaultEncrypt(string text, Encoding encoding)
        {
            byte[] buffer = encoding.GetBytes(text);
            return TripleDESEncrypt(buffer
                , Format(DEFAULT_KEY)
                , Format(DEFAULT_IV));
        }

        /// <summary>
        /// Use the 3DES to encrypt text with default key and encode(UTF-16)
        /// </summary>
        /// <param name="text">Text content</param>
        /// <returns>Encrypted</returns>
        public static string DefaultEncrypt(string text)
        {
            return DefaultEncrypt(text, Encoding.Unicode);
        }

        /// <summary>
        /// Decrypt encrypted by default key
        /// </summary>
        /// <param name="encrypted">Encrypted</param>
        /// <param name="encoding">Encoding</param>
        /// <returns>Decryption</returns>
        public static string DefaultDecrypt(string encrypted, Encoding encoding)
        {
            byte[] buffer = TripleDESDecrypt(encrypted
                , Format(DEFAULT_KEY)
                , Format(DEFAULT_IV));
            return buffer == null ? null : encoding.GetString(buffer).Trim('\0');
        }

        /// <summary>
        /// Decrypt encrypted by default key and encode(UTF-16)
        /// </summary>
        /// <param name="encrypted">Encrypted</param>
        /// <returns>Decryption</returns>
        public static string DefaultDecrypt(string encrypted)
        {
            return DefaultDecrypt(encrypted, Encoding.Unicode);
        }
        #endregion
    }
}
