﻿using System;
using System.Linq;
using iFramework.Framework.Contract;
using System.Collections.Generic;
using iFramework.Framework.Utility;
using System.ComponentModel.DataAnnotations.Schema;

namespace iFramework.Account.Contract
{
    [Auditable]//Auditable属性用于该表在由CUD操作时，将操作记录存到数据库
    [Serializable]
    [Table("LoginInfo")]
    public class LoginInfo : ModelBase
    {
        public LoginInfo()
        {
            LastAccessTime = DateTime.Now;
            LoginToken = Guid.NewGuid();
        }

        public LoginInfo(int userID, string loginName)
        {
            LastAccessTime = DateTime.Now;
            LoginToken = Guid.NewGuid();

            UserID = userID;
            LoginName = loginName;
        }

        private Guid _loginToken;
        public Guid LoginToken
        {
            get { return _loginToken; }
            set { _loginToken = value; }

        }

        private DateTime _lastAccessTime;
        public DateTime LastAccessTime {
            get { return _lastAccessTime; }
            set { _lastAccessTime = value; }
        }

        private int _userID;
        public int UserID {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _loginName;
        public string LoginName {
            get { return _loginName; }
            set { _loginName = value; }
        }

        private string _clientIP;
        public string ClientIP {
            get { return _clientIP; }
            set { _clientIP = value; }
        }

        private int _enumLoginAccountType;
        public int EnumLoginAccountType {
            get { return _enumLoginAccountType; }
            set { _enumLoginAccountType = value; }
        }

        private string _enumLoginResourceType;
        public string EnumLoginResourceType {
            get { return _enumLoginResourceType; }
            set { _enumLoginResourceType = value; }
        }

        private string _businessPermissionString;
        public string BusinessPermissionString {
            get { return _businessPermissionString; }
            set { _businessPermissionString = value; }
        }

        [NotMapped]
        public List<EnumBusinessPermission> BusinessPermissionList
        {
            get
            {
                if (string.IsNullOrEmpty(BusinessPermissionString))
                    return new List<EnumBusinessPermission>();
                else
                    return BusinessPermissionString.Split(",".ToCharArray()).Select(p => int.Parse(p)).Cast<EnumBusinessPermission>().ToList();
            }
            set
            {
                BusinessPermissionString = string.Join(",", value.Select(p => (int)p));
            }
        }
    }

    [Flags]
    public enum EnumLoginAccountType
    {
        [EnumTitle("[无]", IsDisplay = false)]
        Guest = 0,
        /// <summary>
        /// 管理员
        /// </summary>
        [EnumTitle("管理员")]
        Administrator = 1,
    }

    public enum EnumLoginResourceType
    {
        [EnumTitle("[无]", IsDisplay = false)]
        Guest = 0,
        [EnumTitle("后台管理系统")]
        WebManage = 1,
        [EnumTitle("接口平台")]
        WebApi = 2,
    }
}



