﻿using System;
using System.Linq;
using iFramework.Framework.Contract;
using System.Collections.Generic;
using iFramework.Framework.Utility;
using System.ComponentModel.DataAnnotations.Schema;

namespace iFramework.Account.Contract
{
    [Auditable]//Auditable属性用于该表在由CUD操作时，将操作记录存到数据库
    [Serializable]
    [Table("VerifyCode")]
    public class VerifyCode : ModelBase
    {
        private Guid _guid;
        public Guid Guid {
            get { return _guid; }
            set { _guid = value; }
        }

        private string _verifyText;
        public string VerifyText {
            get { return _verifyText; }
            set { _verifyText = value; }
        }
    }

}



