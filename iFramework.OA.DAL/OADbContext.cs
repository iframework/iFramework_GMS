﻿using System;
using iFramework.Framework.DAL;
using iFramework.Core.Config;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using iFramework.OA.Contract;
using iFramework.Core.Log;

namespace iFramework.OA.DAL
{
    public class OADbContext : DbContextBase
    {
        public OADbContext()
            : base(CachedConfigContext.Current.DaoConfig.OA, new LogDbContext())
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<OADbContext>(null);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Staff> Staffs { get; set; }
        public DbSet<Branch> Branchs { get; set; }
    }
}
